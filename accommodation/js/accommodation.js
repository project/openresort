Drupal.accommodationAttach = function() {
  $('#accommodation-unit-block')
    .append('<input type="hidden" name="accommodation" value="1" />')
    .each(function () {
      var elt = new Drupal.accommodation(this);
    });
}

Drupal.accommodation = function(form) {
  Drupal.redirectFormSubmit($(form).attr('action'), form, this);
}

/**
 * Handler for the form redirection submission.
 */
Drupal.accommodation.prototype.onsubmit = function () {
  // Insert progressbar.
  // Insert progressbar and stretch to take the same space.
  this.progress = new Drupal.progressBar('ajaxeditprogress');
  this.progress.setProgress(-1, 'Fetching results');
  var el = this.progress.element;
  $(el).css({
    width: '250px',
    height: '15px',
    paddingTop: '10px',
	paddingBottom:'20px'
  });
  $('div.availability')
    .html('')
    .append(el)
    .fadeIn('slow');
}

/**
 * Handler for the form redirection completion.
 */
Drupal.accommodation.prototype.oncomplete = function (data) {
  //$(this.progress.element).remove();
  //this.progress = null;
  $('div.availability')
    .hide()
    .html(data)
    .end()
    .fadeIn('fast');
  document.getElementById('search-results').scrollIntoView();
}

/**
 * Handler for the form redirection error.
 */
Drupal.accommodation.prototype.onerror = function (error) {
  alert(error);
  $(this.progress.element).fadeOut('slow', function() {
      $(this).remove();
    });
  this.progress = null;
}

if (Drupal.jsEnabled) {
  $(document).ready(Drupal.accommodationAttach);
}