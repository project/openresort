if (Drupal.jsEnabled) {
  $(document).ready(function(){
	Drupal.unitAvailability();
	Drupal.unitSubmitAvailability();
})
};

Drupal.unitAvailability = function(){
	$('a.months').each(function () {
	  $(this).click(function(){
		  html = $('.main-title').html();
		  $('.main-title')
		    .html(html + ' (Loading)')
		    .fadeIn('slow');
		document.forms['updateUnitAvailability'].availdate.value = this.href.substring(this.href.length-6);	
		$.get(this.href,null,function(data){
		$('#unitavailability').html(data);
		Drupal.unitAvailability();
	   }) 
	   return false;
      });
    });
	$('a.calday').each(function () {
	  $(this).click(function(){
		//extra form href
		if(document.forms['updateUnitAvailability'].startdate.value == ''){
			document.forms['updateUnitAvailability'].startdate.value = this.href.substring(this.href.length-10).replace(/-/g,'/');
		}else{
			document.forms['updateUnitAvailability'].enddate.value = this.href.substring(this.href.length-10).replace(/-/g,'/');
		}
		return false;
	   })
      });
};

Drupal.unitSubmitAvailability = function(){
$('#unit-availability-update-form')
	.append('<input type="hidden" name="availdate" id="availdate" value="" />')
    .each(function () {
      var elt = new Drupal.unitUpdateAvailability(this);
    });
}

Drupal.unitUpdateAvailability = function(form) {
  Drupal.redirectFormSubmit($(form).attr('action'), form, this);
}

/**
 * Handler for the form redirection submission.
 */
Drupal.unitUpdateAvailability.prototype.onsubmit = function () {
  // Insert progressbar.
  // Insert progressbar and stretch to take the same space.
  this.progress = new Drupal.progressBar('ajaxeditprogress');
  this.progress.setProgress(-1, 'Updating availability');
  var el = this.progress.element;
  $(el).css({
    width: '250px',
    height: '15px',
    paddingTop: '10px',
	paddingBottom:'20px'
  });
  $('.ajaxmessage')
    .html('')
    .append(el)
    .fadeIn('slow');
}

/**
 * Handler for the form redirection completion.
 */
Drupal.unitUpdateAvailability.prototype.oncomplete = function (data) {
 $(this.progress.element).remove();
 this.progress = null;
  $('#unitavailability')
    .hide()
    .html(data)
    .end()
    .fadeIn('fast');
 $('.ajaxmessage')
    .html('<div class="message status">Availability Updated</div>')
	.end()
    .fadeIn('fast');
 Drupal.unitAvailability();
 document.forms['updateUnitAvailability'].startdate.value = '';
 document.forms['updateUnitAvailability'].enddate.value ='';
}

/**
 * Handler for the form redirection error.
 */
Drupal.unitUpdateAvailability.prototype.onerror = function (error) {
  alert(error);
  $(this.progress.element).fadeOut('slow', function() {
      $(this).remove();
    });
  this.progress = null;
}