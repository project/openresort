Drupal.unitAttach = function() {
  $('#unit-availability-block')
    .append('<input type="hidden" name="unit" value="1" />')
    .each(function () {
      var elt = new Drupal.unit(this);
    });
}

Drupal.unit = function(form) {
  Drupal.redirectFormSubmit($(form).attr('action'), form, this);
}

/**
 * Handler for the form redirection submission.
 */
Drupal.unit.prototype.onsubmit = function () {
  // Insert progressbar.
  // Insert progressbar and stretch to take the same space.
  this.progress = new Drupal.progressBar('ajaxeditprogress');
  this.progress.setProgress(-1, 'Fetching results');
  var el = this.progress.element;
  $(el).css({
    width: '250px',
    height: '15px',
    paddingTop: '10px',
	paddingBottom:'20px'
  });
  $('div.availability')
    .html('')
    .append(el)
    .fadeIn('slow');
}

/**
 * Handler for the form redirection completion.
 */
Drupal.unit.prototype.oncomplete = function (data) {
  //$(this.progress.element).remove();
  //this.progress = null;
  $('div.availability')
    .hide()
    .html(data)
    .end()
    .fadeIn('fast');
  document.getElementById('search-results').scrollIntoView();
}

/**
 * Handler for the form redirection error.
 */
Drupal.unit.prototype.onerror = function (error) {
  alert(error);
  $(this.progress.element).fadeOut('slow', function() {
      $(this).remove();
    });
  this.progress = null;
}

if (Drupal.jsEnabled) {
  $(document).ready(Drupal.unitAttach);
}